<?php

namespace App\Models;
use CodeIgniter\Model;

class M_Testimony extends Model
{
    //$table adalah nama tabel yang akan digunakan oleh Model;
    protected $table        = 'testimoni';
    //$primaryKey adalah nama kolom untuk primary key;
    protected $primarykey   = 'id';
    //$useAutoIncrement untuk mengaktifkan auto increment pada primary key;
    protected $useAutoIncrement = true;
    //$allowedFields berisi daftar nama kolom yang boleh kita isi.
    protected $allowedFields    = ['testimoni_img','testimoni_person','testimoni_title','testimoni_content'];

}

?>