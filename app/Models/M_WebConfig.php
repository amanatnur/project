<?php

namespace App\Models;
use CodeIgniter\Model;

class M_WebConfig extends Model
{
    //$table adalah nama tabel yang akan digunakan oleh Model;
    protected $table        = 'config_web';
    //$primaryKey adalah nama kolom untuk primary key;
    protected $primarykey   = 'id';
    //$useAutoIncrement untuk mengaktifkan auto increment pada primary key;
    protected $useAutoIncrement = true;
    //$allowedFields berisi daftar nama kolom yang boleh kita isi.
    protected $allowedFields    = ['config_name','config_addres','config_logo','config_icon','config_contact','config_contact_alternative','config_address_home','config_email','created_at'];

    // protected $validationRules = [
    //     'config_name' => 'required|trim|min_length[10]|alpha_space',
    //     'config_addres' => 'required|trim|min_length[10]',
    //     'config_contact' => 'required|trim|min_length[10]|numeric',
    //     'config_contact_alternative' => 'required|trim|min_length[10]|numeric',
    //     'config_address_home' => 'required|trim|min_length[10]',
    //     'config_email' => 'required|trim|min_length[10]|valid_email',
        
    // ];
}

?>