<?php

namespace App\Models;
use CodeIgniter\Model;

class M_PageConfig extends Model
{
    //$table adalah nama tabel yang akan digunakan oleh Model;
    protected $table        = 'config_page';
    //$primaryKey adalah nama kolom untuk primary key;
    protected $primarykey   = 'id';
    //$useAutoIncrement untuk mengaktifkan auto increment pada primary key;
    protected $useAutoIncrement = true;
    //$allowedFields berisi daftar nama kolom yang boleh kita isi.
    protected $allowedFields    = [
    'config_name',
    'config_url',
    'config_featured_img',
    'config_heading',
    'config_subheading',
    'config_button',
    'config_link_button',
    'config_link_market',
    'config_fb_ads',
    'config_google_ads',
    'config_meta_key',
    'config_meta_des',
    'testimoni_id'];

    public function getPage()
    {
        $query = $this->db->table('config_page')
                        ->select('config_page.*,testimoni.testimoni_img,testimoni.testimoni_person,testimoni.testimoni_title,testimoni.testimoni_content')
                        ->join('testimoni','config_page.testimoni_id=testimoni.id')
                        ->get();
        return $query;
    }

    public function getPageOne($id=null)
    {
        $query = $this->db->table('config_page')
                        ->select('config_page.*,testimoni.testimoni_img,testimoni.testimoni_person,testimoni.testimoni_title,testimoni.testimoni_content')
                        ->join('testimoni','config_page.testimoni_id=testimoni.id')
                        ->where('config_page.id',$id)
                        ->get();
        return $query;
    }
}
?>