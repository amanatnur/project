<?php

namespace App\Models;
use CodeIgniter\Model;

class M_User extends Model
{
    //$table adalah nama tabel yang akan digunakan oleh Model;
    protected $table        = 'users';
    //$primaryKey adalah nama kolom untuk primary key;
    protected $primarykey   = 'id';
    //$useAutoIncrement untuk mengaktifkan auto increment pada primary key;
    protected $useAutoIncrement = true;
    //$allowedFields berisi daftar nama kolom yang boleh kita isi.
    protected $allowedFields    = ['users_name','user_contact','users_email','users_password','last_login'];

}

?>