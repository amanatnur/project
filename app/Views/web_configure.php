<?= $this->extend('layout/page_layout') ?> 
  <?= $this->section('content') ?>
              <div class="card">
              <?php if(isset($validation)):?>
                    <div class="alert alert-danger"><?= $validation->listErrors() ?></div>
                <?php endif;?>
                <form class="form-horizontal" action="<?= base_url('web_configure_save/'.$config['id'].'')?>" method="post" enctype="multipart/form-data">
                  <div class="card-body">
                    <h4 class="card-title">Web Configure</h4>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Web Name</label
                      >
                      <input type="hidden" name="frm_imglogo" value="<?= $config['config_logo']?>">
                      <input type="hidden" name="frm_imgicon" value="<?= $config['config_icon']?>">
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_web_name"
                          autofocus
                          placeholder="Web Name Here"
                          value="<?= $config['config_name'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Web Address</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="lname"
                          name="frm_web_adds"
                          placeholder="Web Address Here"
                          value="<?= $config['config_addres'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Logo</label
                      >
                      
                      <div class="col-sm-9">
                        <input
                          type="file"
                          class="custom-file-input"
                          id="validatedCustomFile"
                          name="frm_logo"
                          
                          accept="image/*"
                        /><br><br>
                        <div class="col-md-12 col-lg-12 col-xlg-12">
                          <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                              <h1 class="font-light text-white">
                              <img
                                src="<?= base_url('../upload/'.$config['config_logo'].'')?>"
                              />
                              </h1>

                            </div>
                          </div>
                        </div>
                        
                        <!-- <input
                          type="text"
                          class="form-control"
                          id="lname"
                          name="frm_logo"
                          placeholder="Logo Here"
                          value="<?= $config['config_logo'];?>"
                        /> -->
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Favicon</label
                      >
                      <div class="col-sm-9">
                      <input
                          type="file"
                          class="custom-file-input"
                          id="validatedCustomFile"
                          name="frm_icon"
                          accept="image/*"
                          
                        /><br><br>
                        <div class="col-md-12 col-lg-12 col-xlg-12">
                          <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                              <h1 class="font-light text-white">
                                <img
                                src="<?= base_url('../upload/'.$config['config_icon'].'')?>"
                              />
                              </h1>

                            </div>
                          </div>
                        </div>
                        
                        <!-- <input
                          type="text"
                          class="form-control"
                          id="lname"
                          name="frm_icon"
                          placeholder="Favicon Here"
                          value="<?= $config['config_icon'];?>"
                        /> -->
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="email1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >No Telp(Utama)</label
                      >
                      <div class="col-sm-1">
                        <input
                          type="text"
                          class="form-control"
                          id="email1"
                          name="frm_no_tlp1"
                          placeholder="No Telp Name Here"
                          value="+62"
                          readonly
                        />
                      </div>
                      <div class="col-sm-4">
                        <input
                          type="text"
                          class="form-control"
                          id="email1"
                          name="frm_no_trlp"
                          placeholder="No Telp Name Here"
                          value="<?= substr($config['config_contact'],3,15);?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >No Telp(Alternative)</label
                      >
                      <div class="col-sm-1">
                        <input
                          type="text"
                          class="form-control"
                          id="cono1"
                          name="frm_no_altv1"
                          value="+62"
                          readonly
                        />
                      </div>
                      <div class="col-sm-4">
                        <input
                          type="text"
                          class="form-control"
                          id="cono1"
                          name="frm_no_altv"
                          placeholder="Np Telp No Here"
                          value="<?= substr($config['config_contact_alternative'],3,15);?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Email</label
                      >
                      <div class="col-sm-9">
                      <input
                          type="email"
                          class="form-control"
                          id="email1"
                          name="frm_email"
                          placeholder="Email Here"
                          value="<?= $config['config_email'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Address</label
                      >
                      <div class="col-sm-9">
                      <textarea class="form-control" name="frm_addr"><?= $config['config_address_home'];?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="border-top">
                    <div class="card-body">
                      <input type="submit" name="frm_save" value="Save" class="btn btn-primary">
                     
                    </div>
                  </div>
                </form>
              </div>
  <?= $this->endSection() ?>
