<?= $this->extend('layout/page_layout') ?> 
  <?= $this->section('content') ?>
              <div class="card">
              <?php if(isset($validation)):?>
                    <div class="alert alert-danger"><?= $validation->listErrors() ?></div>
                <?php endif;?>
                <form class="form-horizontal" action="<?= base_url('user/password_save') ?>" method="post" enctype="multipart/form-data">
                  <div class="card-body">
                    <h4 class="card-title">Ubah Password</h4>

                    <div class="form-group row">
                    <?php foreach($user as $us):?>
                    <input type="hidden" name="frm_id" value="<?= $us['id']?>">
                    <?php endforeach; ?>
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Password</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="password"
                          class="form-control"
                          id="fname"
                          name="password"
                          autofocus=""
                          
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Confirm Password</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="password"
                          class="form-control"
                          id="email1"
                          name="confpassword"
                        />
                      </div>
                      
                    </div>

                  </div>
                  <div class="border-top">
                    <div class="card-body">
                      <input type="submit" name="frm_save" value="Changes Profil" class="btn btn-primary">
                     
                    </div>
                  </div>
                </form>
              </div>
  <?= $this->endSection() ?>
