<?= $this->extend('layout/page_layout') ?> 
  <?= $this->section('content') ?>
              <div class="card">
              <?php if(isset($validation)):?>
                    <div class="alert alert-danger"><?= $validation->listErrors() ?></div>
                <?php endif;?>
                <form class="form-horizontal" action="<?= base_url('page/save') ?>" method="post" enctype="multipart/form-data">
                  <div class="card-body">
                    <h4 class="card-title">Page Configure</h4>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Testimoni</label
                      >
                      <div class="col-sm-9">
                        <select class="form-control" name="frm_testimoni">
                            <option value="">Pilih Testimoni</option>
                            <?php foreach($testy as $tsy):?>
                            <option value="<?= $tsy['id']?>"><?= $tsy['testimoni_title']?></option> 
                            <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Name</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_name"
                          autofocus=""
                          placeholder="Name Here"
                          value="<?= set_value('frm_name') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >URL</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="lname"
                          name="frm_url"
                          placeholder="Url Here"
                          value="<?= set_value('frm_url') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Image Featured</label
                      >
                      
                      <div class="col-sm-9">
                        <input
                          type="file"
                          class="custom-file-input"
                          id="validatedCustomFile"
                          name="frm_image"
                          value="<?= set_value('frm_image') ?>"
                          accept="image/*"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Heading</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_heading"
                          autofocus=""
                          placeholder="Heading Here"
                          value="<?= set_value('frm_heading') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Subheading</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_subheading"
                          autofocus=""
                          placeholder="subheading Here"
                          value="<?= set_value('frm_subheading') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Button</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_button"
                          autofocus=""
                          placeholder="button Here"
                          value="<?= set_value('frm_button') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Link Button</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_link_button"
                          autofocus=""
                          placeholder="link button Here"
                          value="<?= set_value('frm_link_button') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Link Market</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_link_market"
                          autofocus=""
                          placeholder="link market Here"
                          value="<?= set_value('frm_link_market') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >FB ADS</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_fb"
                          autofocus=""
                          placeholder="fb ads Here"
                          value="<?= set_value('frm_fb') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Google ADS</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_google"
                          autofocus=""
                          placeholder="google Ads Here"
                          value="<?= set_value('frm_google') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Meta Key</label
                      >
                      <div class="col-sm-9">
                      <textarea class="form-control" name="frm_meta"><?= set_value('frm_meta') ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Meta Description</label
                      >
                      <div class="col-sm-9">
                      <textarea class="form-control" name="frm_des"><?= set_value('frm_des') ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="border-top">
                    <div class="card-body">
                      <input type="submit" name="frm_save" value="Save" class="btn btn-primary">
                     
                    </div>
                  </div>
                </form>
              </div>
  <?= $this->endSection() ?>
