<?= $this->extend('layout/page_layout') ?> 
  <?= $this->section('content') ?>
              <div class="card">
              <?php if(isset($validation)):?>
                    <div class="alert alert-danger"><?= $validation->listErrors() ?></div>
                <?php endif;?>
                <form class="form-horizontal" action="<?= base_url('testimoni/edit_save') ?>" method="post" enctype="multipart/form-data">
                  <div class="card-body">
                    <h4 class="card-title">Testimony</h4>
                    <div class="form-group row">
                      <input type="hidden" name="frm_id" value="<?= $testimony['id']?>">
                      <input type="hidden" name="frm_img" value="<?= $testimony['testimoni_img']?>">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Person</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_person"
                          autofocus=""
                          placeholder="Person Here"
                          <?php if(empty(set_value('frm_person'))):?>
                          value="<?= $testimony['testimoni_person']?>"
                          <?php endif; ?>
                          value="<?= set_value('frm_person') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Title</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="lname"
                          name="frm_title"
                          placeholder="Title Here"
                          <?php if(empty(set_value('frm_title'))):?>
                          value="<?= $testimony['testimoni_title']?>"
                          <?php endif; ?>
                          value="<?= set_value('frm_title') ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Image</label
                      >
                      
                      <div class="col-sm-9">
                        <input
                          type="file"
                          class="custom-file-input"
                          id="validatedCustomFile"
                          name="frm_image"
                          value="<?= set_value('frm_image') ?>"
                          accept="image/*"
                        /><br><br>
                        <div class="col-md-12 col-lg-12 col-xlg-12">
                          <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                              <h1 class="font-light text-white">
                              <img
                                src="<?= base_url('../upload/testimony/'.$testimony['testimoni_img'].'')?>"
                                width="100px"
                                height="100px"
                              />
                              </h1>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Content</label
                      >
                      <div class="col-sm-9">
                      <textarea class="form-control" name="frm_content"><?php if(empty(set_value('frm_content'))):?><?= $testimony['testimoni_content']?> <?php endif; ?><?= set_value('frm_content') ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="border-top">
                    <div class="card-body">
                      <input type="submit" name="frm_save" value="Save" class="btn btn-primary">
                     
                    </div>
                  </div>
                </form>
              </div>
  <?= $this->endSection() ?>
