<?= $this->extend('layout/page_layout') ?> 
  <?= $this->section('content') ?>
  <?php if(session()->getFlashdata('msg')):?>
                    <div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
                <?php endif;?>
              <div class="card">
              <?php if(isset($validation)):?>
                    <div class="alert alert-danger"><?= $validation->listErrors() ?></div>
                <?php endif;?>
                <form class="form-horizontal" action="<?= base_url('user/edit_save') ?>" method="post" enctype="multipart/form-data">
                  <div class="card-body">
                    <h4 class="card-title">Profil</h4>
                    <?php foreach($user as $us):?>
                    <div class="form-group row">
                    <input type="hidden" name="frm_id" value="<?= $us['id']?>">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Person</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_person"
                          autofocus=""
                          placeholder="Person Here"
                          value="<?= $us['users_name'] ?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Contact</label
                      >
                      <div class="col-sm-1">
                        <input
                          type="text"
                          class="form-control"
                          id="email1"
                          name="frm_no_tlp1"
                          placeholder="No Telp Name Here"
                          value="+62"
                          readonly
                        />
                      </div>
                      <div class="col-sm-4">
                        <input
                          type="text"
                          class="form-control"
                          id="email1"
                          name="frm_no_trlp"
                          placeholder="No Telp Name Here"
                          value="<?= substr($us['user_contact'],3,15);?>"
                        />
                      </div>
                    </div>

                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Email</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="email"
                          class="form-control"
                          id="lname"
                          name="frm_email"
                          placeholder="Email Here"
                          value="<?= $us['users_email'] ?>"
                        />
                      </div>
                    </div>
                    <?php endforeach; ?>
                  </div>
                  <div class="border-top">
                    <div class="card-body">
                      <input type="submit" name="frm_save" value="Changes Profil" class="btn btn-primary">
                     
                    </div>
                  </div>
                </form>
              </div>
  <?= $this->endSection() ?>
