<link
      href="../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css"
      rel="stylesheet"
    />
<?= $this->extend('layout/page_layout') ?> 
  <?= $this->section('content') ?>    
                <?php if(session()->getFlashdata('msg')):?>
                    <div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
                <?php endif;?>        
            <div class="card">
                <div class="card-body">
                  <h5 class="card-title"><a href="<?= base_url('testimoni/create') ?>" class="btn btn-primary">Add Testimony</a></h5>
                  <div class="table-responsive">
                    <table
                      id="zero_config"
                      class="table table-striped table-bordered"
                    >
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Title</th>
                          <th>Content</th>
                          <th>Image</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($testimony as $test): ?>
                        <tr>
                          <td><?= $test['testimoni_person']; ?></td>
                          <td><?= $test['testimoni_title']; ?></td>
                          <td><?= $test['testimoni_content']; ?></td>
                          <td><img
                                src="<?= base_url('../upload/testimony/'.$test['testimoni_img'].'')?>"
                                width="50px"
                                height="50px"
                              />
                          <td>
                            <a href="<?= base_url('testimoni/'.$test['id'].'/edit') ?>" class="btn btn-success">
                            <i class="fa fa-edit"></i></a>
                            <a href="<?= base_url('testimoni/'.$test['id'].'/delete') ?>" class="btn btn-danger" onclick="return confirm('Yakin Ingin Hapus Data ini?')">
                            <i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                      
                    </table>
                  </div>
                </div>
              </div>
              
<?= $this->endSection() ?>   
       