<link
      href="../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css"
      rel="stylesheet"
    />
<?= $this->extend('layout/page_layout') ?> 
  <?= $this->section('content') ?>   
              <?php if(session()->getFlashdata('msg')):?>
                    <div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
                <?php endif;?>           
            <div class="card">
                <div class="card-body">
                  <h5 class="card-title"><a href="<?= base_url('page/create') ?>" class="btn btn-primary">Add config</a></h5>
                  <div class="table-responsive">
                    <table
                      id="zero_config"
                      class="table table-striped table-bordered"
                    >
                      <thead>
                        <tr>
                           <th>name</th>
                           <th>url</th>
                           <th>featured image</th>
                           <th>heading</th>
                           <th>subheading</th>
                           <th>button</th>
                           <th>link_button</th>
                           <th>link_market</th>
                           <th>fb ads</th>
                           <th>google ads</th>
                           <th>meta key</th>
                           <th>meta description</th>
                           <th>testimoni imgage</th>
                           <th>testimoni person</th>
                           <th>testimoni title</th>
                           <th>testimoni content</th>
                           <th>action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($page as $pg): ?>
                        <tr>
                            <td><?= $pg['config_name']; ?></td> 
                            <td><?= $pg['config_url']; ?></td> 
                            <td><img
                                src="<?= base_url('../upload/pages/'.$pg['config_featured_img'].'')?>"
                                width="50px"
                                height="50px"/></td> 
                            <td><?= $pg['config_heading']; ?></td> 
                            <td><?= $pg['config_subheading']; ?></td> 
                            <td><?= $pg['config_button']; ?></td> 
                            <td><?= $pg['config_link_button']; ?></td> 
                            <td><?= $pg['config_link_market']; ?></td> 
                            <td><?= $pg['config_fb_ads']; ?></td> 
                            <td><?= $pg['config_google_ads']; ?></td> 
                            <td><?= $pg['config_meta_key']; ?></td> 
                            <td><?= $pg['config_meta_des']; ?></td> 
                            <td><img
                                src="<?= base_url('../upload/testimony/'.$pg['testimoni_img'].'')?>"
                                width="50px"
                                height="50px"/>
                            </td>
                            <td><?= $pg['testimoni_person']; ?></td>
                            <td><?= $pg['testimoni_title']; ?></td> 
                            <td><?= $pg['testimoni_content']; ?></td>
                          <td>
                            <a href="<?= base_url('page/'.$pg['id'].'/edit') ?>" class="btn btn-success">
                            <i class="fa fa-edit"></i></a>
                            <a href="<?= base_url('page/'.$pg['id'].'/delete') ?>" class="btn btn-danger" onclick="return confirm('Yakin Ingin Hapus Data ini?')">
                            <i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                      
                    </table>
                  </div>
                </div>
              </div>
              
<?= $this->endSection() ?>   
       