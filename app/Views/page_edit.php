<?= $this->extend('layout/page_layout') ?> 
  <?= $this->section('content') ?>
              <div class="card">
              <?php if(isset($validation)):?>
                    <div class="alert alert-danger"><?= $validation->listErrors() ?></div>
                <?php endif;?>
                <form class="form-horizontal" action="<?= base_url('page/edit_save') ?>" method="post" enctype="multipart/form-data">
                  <div class="card-body">
                    <h4 class="card-title">Page Configure</h4>
                    <?php foreach($page as $pg):?>
                      <input type="hidden" name="frm_id" value="<?= $pg['id']?>">
                      <input type="hidden" name="frm_img" value="<?= $pg['config_featured_img']?>">
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Testimoni</label
                      >
                      <div class="col-sm-9">
                        <select class="form-control" name="frm_testimoni">
                            <option value="">Pilih Testimoni</option>
                            <?php foreach($testy as $tsy):?>
                            <option value="<?= $tsy['id']?>" <?php if($tsy['id']==$pg['testimoni_id']){echo "selected";}?> ><?= $tsy['testimoni_title']?></option> 
                            <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Name</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_name"
                          autofocus=""
                          placeholder="Name Here"
                          value="<?= $pg['config_name'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >URL</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="lname"
                          name="frm_url"
                          placeholder="Url Here"
                          value="<?= $pg['config_url'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="lname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Image Featured</label
                      >
                      
                      <div class="col-sm-9">
                        <input
                          type="file"
                          class="custom-file-input"
                          id="validatedCustomFile"
                          name="frm_image"
                          value="<?= $pg['config_featured_img'];?>"
                          accept="image/*"
                        /><br><br>
                        <div class="col-md-12 col-lg-12 col-xlg-12">
                          <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                              <h1 class="font-light text-white">
                              <img
                                src="<?= base_url('../upload/pages/'.$pg['config_featured_img'].'')?>"
                                width="100px"
                                height="100px"
                              />
                              </h1>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Heading</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_heading"
                          autofocus=""
                          placeholder="Heading Here"
                          value="<?= $pg['config_heading'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Subheading</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_subheading"
                          autofocus=""
                          placeholder="subheading Here"
                          value="<?= $pg['config_subheading'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Button</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_button"
                          autofocus=""
                          placeholder="button Here"
                          value="<?= $pg['config_button'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Link Button</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_link_button"
                          autofocus=""
                          placeholder="link button Here"
                          value="<?= $pg['config_link_button'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Link Market</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_link_market"
                          autofocus=""
                          placeholder="link market Here"
                          value="<?= $pg['config_link_market'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >FB ADS</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_fb"
                          autofocus=""
                          placeholder="fb ads Here"
                          value="<?= $pg['config_fb_ads'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="fname"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Google ADS</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          class="form-control"
                          id="fname"
                          name="frm_google"
                          autofocus=""
                          placeholder="google Ads Here"
                          value="<?= $pg['config_google_ads'];?>"
                        />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Meta Key</label
                      >
                      <div class="col-sm-9">
                      <textarea class="form-control" name="frm_meta"><?= $pg['config_meta_key'];?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label
                        for="cono1"
                        class="col-sm-3 text-end control-label col-form-label"
                        >Meta Description</label
                      >
                      <div class="col-sm-9">
                      <textarea class="form-control" name="frm_des"><?= $pg['config_meta_des'];?></textarea>
                      </div>
                    </div>
                    <?php endforeach; ?>
                  </div>
                  <div class="border-top">
                    <div class="card-body">
                      <input type="submit" name="frm_save" value="Save" class="btn btn-primary">
                     
                    </div>
                  </div>
                </form>
              </div>
  <?= $this->endSection() ?>
