<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/dashboard', 'C_WebConfig::index',['filter' => 'auth']);
$routes->add('/web_configure/(:any)', 'C_WebConfig::web_configure/$1',['filter' => 'auth']);
$routes->add('/web_configure_save/(:any)', 'C_WebConfig::web_configure_save/$1',['filter' => 'auth']);

//testimony
$routes->get('/testimoni', 'C_Testimony::index',['filter' => 'auth']);
$routes->get('/testimoni/create', 'C_Testimony::create',['filter' => 'auth']);
$routes->add('/testimoni/save', 'C_Testimony::save',['filter' => 'auth']);
$routes->add('testimoni/(:segment)/edit', 'C_Testimony::edit/$1',['filter' => 'auth']);
$routes->add('/testimoni/edit_save', 'C_Testimony::edit_save',['filter' => 'auth']);
$routes->get('testimoni/(:segment)/delete', 'C_Testimony::delete/$1',['filter' => 'auth']);

//page_configure
$routes->get('/page_configure', 'C_PageConfig::index',['filter' => 'auth']);
$routes->get('/page/create', 'C_PageConfig::create',['filter' => 'auth']);
$routes->add('/page/save', 'C_PageConfig::save',['filter' => 'auth']);
$routes->add('page/(:segment)/edit', 'C_PageConfig::edit/$1',['filter' => 'auth']);
$routes->add('/page/edit_save', 'C_PageConfig::edit_save',['filter' => 'auth']);
$routes->get('page/(:segment)/delete', 'C_PageConfig::delete/$1',['filter' => 'auth']);

//user
$routes->get('/', 'C_User::login');
$routes->get('/user/account', 'C_User::password',['filter' => 'auth']);
$routes->add('/user/password_save', 'C_User::password_save',['filter' => 'auth']);
$routes->add('/user/check_login', 'C_User::check_login');
$routes->get('/user', 'C_User::index',['filter' => 'auth']);
$routes->add('/user/edit_save', 'C_User::edit_save',['filter' => 'auth']);
$routes->add('/user/password', 'C_User::password',['filter' => 'auth']);
$routes->add('/user/logout', 'C_User::logout');

//home
$routes->get('/data_web_configure', 'Home::data_web_configure',['filter' => 'auth']);
$routes->get('/data_user', 'Home::data_user',['filter' => 'auth']);
$routes->get('/data_testimoni', 'Home::data_testimoni',['filter' => 'auth']);
$routes->get('/data_testimoniOne/(:any)', 'Home::data_testimoniOne/$1',['filter' => 'auth']);
$routes->get('/data_page', 'Home::data_page',['filter' => 'auth']);
$routes->get('/data_pageOne/(:any)', 'Home::data_pageOne/$1',['filter' => 'auth']);
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
