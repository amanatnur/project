<?php

namespace App\Controllers;
use \App\Models\M_User;
use CodeIgniter\Exceptions\PageNotFoundException;

class C_User extends BaseController
{
    public function index()
    {
        helper(['form']);
        $model = new M_User();
        $data['user'] = $model->find();
        //echo '<pre>'; print_r($data);
        return view('user',$data);
    }

    public function password()
    {
        helper(['form']);
        $model = new M_User();
        $data['user'] = $model->find();
        //echo '<pre>'; print_r($data);
        return view('password',$data);
    }

    public function login()
    {
        helper(['form']);
        //echo 'hello';
        return view('login');
    }

    public function check_login()
    {
        $model = new M_User();
        $this->session = session();
        $email = $this->request->getVar('frm_email');
        $password = $this->request->getVar('frm_pass');
        $data = $model->where('users_email', $email)->first();
        $arraydata = array(
            'users_email' => $this->request->getPost('frm_email')
        );
        //khusus untuk captcha
        $recaptchaResponse = trim($this->request->getVar('g-recaptcha-response'));
        $secret='6Ld88nQdAAAAACAGj97an6SSwzGVVv5O6kP1Eo0i';
    
        $credential = array(
                'secret' => $secret,
                'response' => $this->request->getVar('g-recaptcha-response')
            );
    
        $verify = curl_init();
        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($verify);
        $status= json_decode($response, true);
        //cek status captcha apakah bebar atau tidak
        if($status['success']){ 
            //cek password apabila data ditemukan
            if($data){
                
                $pass = $data['users_password'];
                $verify_pass = password_verify($password, $pass);
                //cek password hash verifikasinya
                if($verify_pass){
                    //update ke colom last login
                    $model->update($data['id'], [
                        "last_login"        => date('Y-m-d H:i:s')
                    ]); 

                    $ses_data = [
                        'id'       => $data['id'],
                        'users_name'     => $data['users_name'],
                        'users_email'    => $data['users_email'],
                        'last_login'     => date('Y-m-d H:i:s'),
                        'logged_in'      => TRUE
                    ];
                    $this->session->set($ses_data);
                    return redirect()->to('/dashboard');
                }else{
                    $this->session->setFlashdata($arraydata);
                    $this->session->setFlashdata('msg', 'Password Salah');
                    return redirect()->to('/');
                }
            }else{
                $this->session->setFlashdata($arraydata);
                $this->session->setFlashdata('msg', 'Email tidak ditemukan');
                return redirect()->to('/');
            }
        }else{
            $this->session->setFlashdata($arraydata);
            $this->session->setFlashdata('msg', 'Captcha harus dicentang');
            return redirect()->to('/');
        }
    }
    
    public function edit_save()
    {
        //include helper form
        helper(['form']);
        $model = new M_User();
        $this->session = session();
        $input = $this->request->getPost();       
            $rules = 
                [
                    'frm_person'  => 'required|trim|min_length[4]|alpha_space',
                    'frm_no_trlp'  => 'required|trim|min_length[10]|numeric',
                    'frm_email'   => 'required|trim|min_length[10]|valid_email',
                ];
            
            $required = [
                'users_name'        => $input['frm_person'],
                'user_contact'     => $input['frm_no_tlp1'].$input['frm_no_trlp'],
                'users_email'       => $input['frm_email']
            ];           

        if($this->validate($rules)){
            $this->session->setFlashdata('msg', 'Data Berhasil Dirubah');
            $model->update($input['frm_id'],$required);
            $data['user'] = $model->find();
            return redirect('user',$data);
        }else{
            $data['user'] = $model->find();
            $data['validation'] = $this->validator;
            return view('user', $data);
        }
       
    }

    public function password_save()
    {
        //include helper form
        helper(['form']);
        $model = new M_User();
        $this->session = session();
        $input = $this->request->getPost();       
            $rules = 
                [
                    'password'      => 'required|min_length[6]|trim|max_length[15]|regex_match[/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z]{6,200}$/]',
                    'confpassword'  => 'matches[password]'
                ];
            
            $required = [
                'users_password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT)
            ];           

        if($this->validate($rules)){
            $this->session->setFlashdata('msg', 'Password Berhasil Dirubah Silakan Logout dan Login kembali');
            $model->update($input['frm_id'],$required);
            $data['user'] = $model->find();
            return redirect()->to('/dashboard');
            //return redirect('/dashboard',$data);
        }else{
            $data['user'] = $model->find();
            $data['validation'] = $this->validator;
            return view('password', $data);
        }
       
    }

    public function logout()
    {
        $this->session = session();
        $this->session->destroy();
        return redirect()->to('/');
    }
}