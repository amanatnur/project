<?php

namespace App\Controllers;
use \App\Models\M_WebConfig;
use CodeIgniter\Exceptions\PageNotFoundException;

class C_WebConfig extends BaseController
{
    public function index()
    {
        return view('index');
    }

    public function web_configure($id)
    {
        $model = new M_WebConfig();
        $data['config'] = $model->where('id', $id)->first();
        //echo '<pre>'; print_r($data);
        return view('web_configure',$data);
    }

    public function web_configure_save($id)
    {
        //include helper form
       helper(['form']);
       $model = new M_WebConfig();
       $imagelogo = $this->request->getFile('frm_logo');
       $imageicon = $this->request->getFile('frm_icon');
       $input = $this->request->getPost();

       if($_FILES['frm_logo']['error']=='0' && $_FILES['frm_icon']['error']=='0'){

            //set rules validation form
            $rules = [
                    'frm_web_name'  => 'required|trim|min_length[10]|alpha_space',
                    'frm_web_adds'  => 'required|trim|min_length[10]',
                    'frm_no_trlp'   => 'required|trim|min_length[10]|numeric',
                    'frm_no_altv'   => 'required|trim|min_length[10]|numeric',
                    'frm_addr'      => 'required|trim|min_length[10]',
                    'frm_email'     => 'required|trim|min_length[10]|valid_email',
                    'frm_logo' => [
                        'uploaded[frm_logo]',
                        'mime_in[frm_logo, image/jpg,image/jpeg,image/gif,image/png,image/JPG,image/JPEG,image/GIF,image/PNG]',
                        'max_size[frm_logo,1000]',
                    ],
                    'frm_icon' => [
                        'uploaded[frm_icon]',
                        'mime_in[frm_icon, image/jpg,image/jpeg,image/gif,image/png,image/JPG,image/JPEG,image/GIF,image/PNG]',
                        'max_size[frm_icon,1000]',
                    ],
            ];
            
            $required = [
                    'config_name'       => $input['frm_web_name'],
                    'config_addres'     => $input['frm_web_adds'],
                    'config_logo'       => $imagelogo->getName(),
                    'config_icon'       => $imageicon->getName(),
                    'config_contact'    => $input['frm_no_tlp1'].$input['frm_no_trlp'],
                    'config_contact_alternative'    => $input['frm_no_altv1'].$input['frm_no_altv'],
                    'config_email'                  => $input['frm_email'],
                    'config_address_home'           => $input['frm_addr']
                ];
            }else if($_FILES['frm_icon']['error']=='0'){
            //set rules validation form
            $rules = [
                'frm_web_name'  => 'required|trim|min_length[10]|alpha_space',
                'frm_web_adds'  => 'required|trim|min_length[10]',
                'frm_no_trlp'   => 'required|trim|min_length[10]|numeric',
                'frm_no_altv'   => 'required|trim|min_length[10]|numeric',
                'frm_addr'      => 'required|trim|min_length[10]',
                'frm_email'     => 'required|trim|min_length[10]|valid_email',
                'frm_icon' => [
                    'uploaded[frm_icon]',
                    'mime_in[frm_icon, image/jpg,image/jpeg,image/gif,image/png,image/JPG,image/JPEG,image/GIF,image/PNG]',
                    'max_size[frm_icon,1000]',
                ],
            ];
            
            $required = [
                    'config_name'       => $input['frm_web_name'],
                    'config_addres'     => $input['frm_web_adds'],
                    'config_logo'       => $input['frm_imglogo'],
                    'config_icon'       => $imageicon->getName(),
                    'config_contact'    => $input['frm_no_tlp1'].$input['frm_no_trlp'],
                    'config_contact_alternative'    => $input['frm_no_altv1'].$input['frm_no_altv'],
                    'config_email'                  => $input['frm_email'],
                    'config_address_home'           => $input['frm_addr']
                ];                
            }else if($_FILES['frm_logo']['error']=='0'){
            //set rules validation form
            $rules = [
                'frm_web_name'  => 'required|trim|min_length[10]|alpha_space',
                'frm_web_adds'  => 'required|trim|min_length[10]',
                'frm_no_trlp'   => 'required|trim|min_length[10]|numeric',
                'frm_no_altv'   => 'required|trim|min_length[10]|numeric',
                'frm_addr'      => 'required|trim|min_length[10]',
                'frm_email'     => 'required|trim|min_length[10]|valid_email',
                'frm_logo' => [
                    'uploaded[frm_logo]',
                    'mime_in[frm_logo, image/jpg,image/jpeg,image/gif,image/png,image/JPG,image/JPEG,image/GIF,image/PNG]',
                    'max_size[frm_logo,1000]',
                ],
            ];
            
            $required = [
                    'config_name'       => $input['frm_web_name'],
                    'config_addres'     => $input['frm_web_adds'],
                    'config_logo'       => $imagelogo->getName(),
                    'config_icon'       => $input['frm_imgicon'],
                    'config_contact'    => $input['frm_no_tlp1'].$input['frm_no_trlp'],
                    'config_contact_alternative'    => $input['frm_no_altv1'].$input['frm_no_altv'],
                    'config_email'                  => $input['frm_email'],
                    'config_address_home'           => $input['frm_addr']
                ];
            }else{
             //set rules validation form
             $rules = [
                'frm_web_name'  => 'required|trim|min_length[10]|alpha_space',
                'frm_web_adds'  => 'required|trim|min_length[10]',
                'frm_no_trlp'   => 'required|trim|min_length[10]|numeric',
                'frm_no_altv'   => 'required|trim|min_length[10]|numeric',
                'frm_addr'      => 'required|trim|min_length[10]',
                'frm_email'     => 'required|trim|min_length[10]|valid_email',
            ];
            
            $required = [
                    'config_name'       => $input['frm_web_name'],
                    'config_addres'     => $input['frm_web_adds'],
                    'config_logo'       => $input['frm_imglogo'],
                    'config_icon'       => $input['frm_imgicon'],
                    'config_contact'    => $input['frm_no_tlp1'].$input['frm_no_trlp'],
                    'config_contact_alternative'    => $input['frm_no_altv1'].$input['frm_no_altv'],
                    'config_email'                  => $input['frm_email'],
                    'config_address_home'           => $input['frm_addr']
                ];               
            }

       if($this->validate($rules)){
            $model->update($id, $required);
            if($_FILES['frm_logo']['error']=='0' && $_FILES['frm_icon']['error']=='0'){
                $imagelogo->move(WRITEPATH . '../public/upload/');
                $imageicon->move(WRITEPATH . '../public/upload/');
            }else if($_FILES['frm_icon']['error']=='0'){
                $imageicon->move(WRITEPATH . '../public/upload/');
            }else if($_FILES['frm_logo']['error']=='0'){
                $imagelogo->move(WRITEPATH . '../public/upload/');
            }
            
            return redirect('dashboard');
       }else{
            $data['config'] = $model->where('id', $id)->first();
            $data['validation'] = $this->validator;
            return view('web_configure', $data);
       }
        //return view('web_configure',$data);
    }
}
