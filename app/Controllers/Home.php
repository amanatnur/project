<?php

namespace App\Controllers;
use \App\Models\M_PageConfig;
use \App\Models\M_Testimony;
use \App\Models\M_User;
use \App\Models\M_WebConfig;

class Home extends BaseController
{
    public function index()
    {
        return view('welcome_message');
    }

    //khusus yang dikirim
    public function data_web_configure()
    {
        $model = new M_WebConfig();
        $data['config'] = $model->find();
        echo '<pre>'; print_r($data);
        //return view('home',$data);
    }

    public function data_user()
    {
        helper(['form']);
        $model = new M_User();
        $data['user'] = $model->find();
        echo '<pre>'; print_r($data);
        //return view('home',$data);
    }

    public function data_testimoni()
    {
        $model = new M_Testimony();
        $data['testimony'] = $model->findAll();
        echo '<pre>'; print_r($data);
        //return view('home',$data);
    }
    public function data_testimoniOne($id)
    {
        helper(['form']);
        $model = new M_Testimony();
        $data['testimony'] = $model->where('id', $id)->first();
        echo '<pre>'; print_r($data);
        //return view('home',$data);
    }

    public function data_page()
    {
        helper(['form']);
        $model = new M_PageConfig();
        $data['page']   = $model->getPage()->getResultArray();
        echo '<pre>'; print_r($data);
        //return view('home',$data);
    }

    public function data_pageOne($id)
    {
        helper(['form']);
        $model = new M_PageConfig();
        $data['page'] = $model->getPageOne($id)->getResultArray();
        echo '<pre>'; print_r($data);
        //return view('home',$data);
    }

}
