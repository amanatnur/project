<?php

namespace App\Controllers;
use \App\Models\M_Testimony;
use \App\Models\M_PageConfig;
use CodeIgniter\Exceptions\PageNotFoundException;

class C_Testimony extends BaseController
{
    public function index()
    {
        $model = new M_Testimony();
        $data['testimony'] = $model->findAll();
        return view('testimony',$data);
    }

    public function create()
    {
        helper(['form']);
        return view('testimony_create');
    }

    public function save()
    {
        //include helper form
        helper(['form']);
        $model = new M_Testimony();
        $this->session = session();
        $rules = 
            [
                'frm_person'  => 'required|trim|min_length[4]|alpha_space',
                'frm_title'  => 'required|trim|min_length[10]',
                'frm_content'   => 'required|trim|min_length[10]',
                'frm_image' => [
                    'uploaded[frm_image]',
                    'mime_in[frm_image, image/jpg,image/jpeg,image/gif,image/png,image/JPG,image/JPEG,image/GIF,image/PNG]',
                    'max_size[frm_image,1000]',
                ],
            ];
        $image = $this->request->getFile('frm_image');
        $input = $this->request->getPost();
        $required = [
             'testimoni_person'    => $input['frm_person'],
             'testimoni_title'     => $input['frm_title'],
             'testimoni_img'       => $image->getName(),
             'testimoni_content'   => $input['frm_content']
         ];

        if($this->validate($rules)){
            $model->save($required);
            $image->move(WRITEPATH . '../public/upload/testimony');
            $this->session->setFlashdata('msg', 'Data Berhasil ditambah');
            $data['testimony'] = $model->findAll();
            //return view('testimony',$data);
            return redirect('testimoni',$data);
        }else{
                $data['validation'] = $this->validator;
                return view('testimony_create', $data);
        }
       
    }

    public function edit($id)
    {
        helper(['form']);
        $model = new M_Testimony();
        $data['testimony'] = $model->where('id', $id)->first();
        return view('testimony_edit',$data);
    }

    public function edit_save()
    {
        //include helper form
        helper(['form']);
        $model = new M_Testimony();
        $this->session = session();
        $image = $this->request->getFile('frm_image');
        $input = $this->request->getPost();

        if($_FILES['frm_image']['error']=='0'){
            $rules = 
                [
                    'frm_person'  => 'required|trim|min_length[4]|alpha_space',
                    'frm_title'  => 'required|trim|min_length[10]',
                    'frm_content'   => 'required|trim|min_length[10]',
                    'frm_image' => [
                        'uploaded[frm_image]',
                        'mime_in[frm_image, image/jpg,image/jpeg,image/gif,image/png,image/JPG,image/JPEG,image/GIF,image/PNG]',
                        'max_size[frm_image,1000]',
                    ],
                ];
            
            $required = [
                'testimoni_person'    => $input['frm_person'],
                'testimoni_title'     => $input['frm_title'],
                'testimoni_img'       => $image->getName(),
                'testimoni_content'   => $input['frm_content']
            ];
        }else{
            $rules = 
                [
                    'frm_person'  => 'required|trim|min_length[4]|alpha_space',
                    'frm_title'  => 'required|trim|min_length[10]',
                    'frm_content'   => 'required|trim|min_length[10]',
                ];
            
            $required = [
                'testimoni_person'    => $input['frm_person'],
                'testimoni_title'     => $input['frm_title'],
                'testimoni_img'       => $input['frm_img'],
                'testimoni_content'   => $input['frm_content']
            ];           
        }

        if($this->validate($rules)){
            $model->update($input['frm_id'],$required);
            if($_FILES['frm_image']['error']=='0'){
                $image->move(WRITEPATH . '../public/upload/testimony');
            }
            $this->session->setFlashdata('msg', 'Data Berhasil dirubah');
            $data['testimony'] = $model->findAll();
            //return view('testimony',$data);
            return redirect('testimoni',$data);
            // echo '<pre>'; print_r($_FILES);
            // echo '<pre>'; print_r($required);
        }else{
                $data['testimony'] = $model->where('id', $input['frm_id'])->first();
                $data['validation'] = $this->validator;
                return view('testimony_edit', $data);
                // echo '<pre>'; print_r($_FILES);
                // echo '<pre>'; print_r($required);
        }
       
    }

    public function delete($id)
    {
        $model = new M_Testimony();
        $model_prod = new M_PageConfig();
        $this->session = session();
        $page = $model_prod->where('testimoni_id', $id)->first();
        if(isset($page['testimoni_id'])){
            $this->session->setFlashdata('msg', 'Id sedang digunakan dan tidak bisa dihapus');
            $data['testimony'] = $model->findAll();
            return redirect('testimoni',$data);
        }
        //echo '<pre>'; print_r($page['testimoni_id']);
        $this->session->setFlashdata('msg', 'Data Berhasil dihapus');
        $model->delete($id);
        $data['testimony'] = $model->findAll();
        return redirect('testimoni',$data);
    }
}