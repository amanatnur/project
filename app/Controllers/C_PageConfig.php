<?php

namespace App\Controllers;
use \App\Models\M_PageConfig;
use \App\Models\M_Testimony;
use CodeIgniter\Exceptions\PageNotFoundException;

class C_PageConfig extends BaseController
{
    public function index()
    {
        $model = new M_PageConfig();
        $data['page']   = $model->getPage()->getResultArray();
        //echo '<pre>'; print_r($data);
        return view('page_configure',$data);
    }

    public function create()
    {
        helper(['form']);
        $testy = new M_Testimony();
        $data['testy']  = $testy->findAll();
        return view('page_create',$data);
    }

    public function save()
    {
        //include helper form
        helper(['form']);
        $model = new M_PageConfig();
        $testy = new M_Testimony();
        $this->session = session();
        $rules = 
            [
                'frm_name'      => 'required|trim',
                'frm_url'       => 'required|trim',
                'frm_heading'   => 'required|trim',
                'frm_subheading'    => 'required|trim',
                'frm_button'        => 'required|trim',
                'frm_link_button'   => 'required|trim',
                'frm_link_market'   => 'required|trim',
                'frm_fb'        => 'required|trim',
                'frm_google'    => 'required|trim',
                'frm_meta'      => 'required|trim',
                'frm_des'       => 'required|trim',
                'frm_testimoni' => 'required|trim',
                'frm_image' => [
                    'uploaded[frm_image]',
                    'mime_in[frm_image, image/jpg,image/jpeg,image/gif,image/png,image/JPG,image/JPEG,image/GIF,image/PNG]',
                    'max_size[frm_image,1000]',
                ],
            ];
        $image = $this->request->getFile('frm_image');
        $input = $this->request->getPost();

        $required = [
            'config_name'           => $input['frm_name'],
            'config_url'            => $input['frm_url'],
            'config_featured_img'   => $image->getName(),
            'config_heading'        => $input['frm_heading'],
            'config_subheading'     => $input['frm_subheading'],
            'config_button'         => $input['frm_button'],
            'config_link_button'    => $input['frm_link_button'],
            'config_link_market'    => $input['frm_link_market'],
            'config_fb_ads'         => $input['frm_fb'],
            'config_google_ads'     => $input['frm_google'],
            'config_meta_key'       => $input['frm_meta'],
            'config_meta_des'	    => $input['frm_des'],
            'testimoni_id' 	        => $input['frm_testimoni'],
         ];

        if($this->validate($rules)){
            $model->save($required);
            $image->move(WRITEPATH . '../public/upload/pages');
            $data['page']   = $model->getPage()->getResultArray();
            //echo '<pre>'; print_r($data);
            $this->session->setFlashdata('msg', 'Data Berhasil Ditambahkan');
            return redirect('page_configure',$data);
        }else{
                $data['testy']  = $testy->findAll();
                $data['validation'] = $this->validator;
                return view('page_create', $data);
        }
    }

    public function edit($id)
    {
        helper(['form']);
        $model = new M_PageConfig();
        $testy = new M_Testimony();
        $data['testy']  = $testy->findAll();
        $data['page'] = $model->getPageOne($id)->getResultArray();
        return view('page_edit',$data);
        //echo '<pre>'; print_r($data);
    }

    public function edit_save()
    {
        //include helper form
        helper(['form']);
        $model = new M_PageConfig();
        $testy = new M_Testimony();
        $this->session = session();
        $image = $this->request->getFile('frm_image');
        $input = $this->request->getPost();

        if($_FILES['frm_image']['error']=='0'){
            $rules = 
                [
                    'frm_name'      => 'required|trim',
                    'frm_url'       => 'required|trim',
                    'frm_heading'   => 'required|trim',
                    'frm_subheading'    => 'required|trim',
                    'frm_button'        => 'required|trim',
                    'frm_link_button'   => 'required|trim',
                    'frm_link_market'   => 'required|trim',
                    'frm_fb'        => 'required|trim',
                    'frm_google'    => 'required|trim',
                    'frm_meta'      => 'required|trim',
                    'frm_des'       => 'required|trim',
                    'frm_testimoni' => 'required|trim',
                    'frm_image' => [
                        'uploaded[frm_image]',
                        'mime_in[frm_image, image/jpg,image/jpeg,image/gif,image/png,image/JPG,image/JPEG,image/GIF,image/PNG]',
                        'max_size[frm_image,1000]',
                    ],
                ];
            
            $required = [
                'config_name'           => $input['frm_name'],
                'config_url'            => $input['frm_url'],
                'config_featured_img'   => $image->getName(),
                'config_heading'        => $input['frm_heading'],
                'config_subheading'     => $input['frm_subheading'],
                'config_button'         => $input['frm_button'],
                'config_link_button'    => $input['frm_link_button'],
                'config_link_market'    => $input['frm_link_market'],
                'config_fb_ads'         => $input['frm_fb'],
                'config_google_ads'     => $input['frm_google'],
                'config_meta_key'       => $input['frm_meta'],
                'config_meta_des'	    => $input['frm_des'],
                'testimoni_id' 	        => $input['frm_testimoni'],
            ];
        }else{
            $rules = 
                [
                    'frm_name'      => 'required|trim',
                    'frm_url'       => 'required|trim',
                    'frm_heading'   => 'required|trim',
                    'frm_subheading'    => 'required|trim',
                    'frm_button'        => 'required|trim',
                    'frm_link_button'   => 'required|trim',
                    'frm_link_market'   => 'required|trim',
                    'frm_fb'        => 'required|trim',
                    'frm_google'    => 'required|trim',
                    'frm_meta'      => 'required|trim',
                    'frm_des'       => 'required|trim',
                    'frm_testimoni' => 'required|trim',
                ];
            
            $required = [
                'config_name'           => $input['frm_name'],
                'config_url'            => $input['frm_url'],
                'config_featured_img'   => $input['frm_img'],
                'config_heading'        => $input['frm_heading'],
                'config_subheading'     => $input['frm_subheading'],
                'config_button'         => $input['frm_button'],
                'config_link_button'    => $input['frm_link_button'],
                'config_link_market'    => $input['frm_link_market'],
                'config_fb_ads'         => $input['frm_fb'],
                'config_google_ads'     => $input['frm_google'],
                'config_meta_key'       => $input['frm_meta'],
                'config_meta_des'	    => $input['frm_des'],
                'testimoni_id' 	        => $input['frm_testimoni'],
            ];           
        }

        if($this->validate($rules)){
            $model->update($input['frm_id'],$required);
            if($_FILES['frm_image']['error']=='0'){
                $image->move(WRITEPATH . '../public/upload/pages');
            }
            $this->session->setFlashdata('msg', 'Data Berhasil dirubah');
            $data['page']   = $model->getPage()->getResultArray();
            //return view('testimony',$data);
            return redirect('page_configure',$data);
            // echo '<pre>'; print_r($_FILES);
            // echo '<pre>'; print_r($required);
        }else{
                $data['testy']  = $testy->findAll();
                $data['page']   = $model->getPageOne($input['frm_id'])->getResultArray();
                $data['validation'] = $this->validator;
                return view('page_edit', $data);
                // echo '<pre>'; print_r($_FILES);
                // echo '<pre>'; print_r($required);
        }
       
    }

    public function delete($id)
    {
        $model = new M_PageConfig();
        $model->delete($id);
        $this->session = session();
        $this->session->setFlashdata('msg', 'Data Berhasil dihapus');
        $data['page']   = $model->getPage()->getResultArray();
        return redirect('page_configure',$data);
    }
}