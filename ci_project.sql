-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2021 at 01:30 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `config_page`
--

CREATE TABLE `config_page` (
  `id` int(11) NOT NULL,
  `config_name` varchar(100) NOT NULL,
  `config_url` varchar(100) NOT NULL,
  `config_featured_img` varchar(50) NOT NULL,
  `config_heading` varchar(50) NOT NULL,
  `config_subheading` varchar(50) NOT NULL,
  `config_button` varchar(50) NOT NULL,
  `config_link_button` varchar(100) NOT NULL,
  `config_link_market` varchar(100) NOT NULL,
  `config_fb_ads` varchar(100) NOT NULL,
  `config_google_ads` varchar(100) NOT NULL,
  `config_meta_key` mediumtext NOT NULL,
  `config_meta_des` mediumtext NOT NULL,
  `testimoni_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `config_page`
--

INSERT INTO `config_page` (`id`, `config_name`, `config_url`, `config_featured_img`, `config_heading`, `config_subheading`, `config_button`, `config_link_button`, `config_link_market`, `config_fb_ads`, `config_google_ads`, `config_meta_key`, `config_meta_des`, `testimoni_id`) VALUES
(19, 'About Us', 'FrontPage/about', 'error-bg.jpg', 'About us', 'About us', 'btn btn-primary', 'FrontPage/link-button', 'FrontPage/link-market', 'facebook', 'google', 'Meta Keyword', 'Meta Description', 2);

-- --------------------------------------------------------

--
-- Table structure for table `config_web`
--

CREATE TABLE `config_web` (
  `id` int(11) NOT NULL,
  `config_name` varchar(100) NOT NULL,
  `config_addres` varchar(100) NOT NULL,
  `config_logo` varchar(50) NOT NULL,
  `config_icon` varchar(50) NOT NULL,
  `config_contact` char(15) NOT NULL,
  `config_contact_alternative` char(15) NOT NULL,
  `config_address_home` mediumtext NOT NULL,
  `config_email` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `config_web`
--

INSERT INTO `config_web` (`id`, `config_name`, `config_addres`, `config_logo`, `config_icon`, `config_contact`, `config_contact_alternative`, `config_address_home`, `config_email`, `created_at`) VALUES
(1, 'Seputar Sepak Bola Indonesia', 'frontPage/news', 'logo.png', 'logo-icon.png', '+6289672176132', '+6289672176130', 'Yogyakarta Istimewa', 'nkichirou@gmail.com', '2021-12-08 06:00:02');

-- --------------------------------------------------------

--
-- Table structure for table `testimoni`
--

CREATE TABLE `testimoni` (
  `id` int(11) NOT NULL,
  `testimoni_img` varchar(50) NOT NULL,
  `testimoni_person` varchar(50) NOT NULL,
  `testimoni_title` varchar(100) NOT NULL,
  `testimoni_content` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `testimoni`
--

INSERT INTO `testimoni` (`id`, `testimoni_img`, `testimoni_person`, `testimoni_title`, `testimoni_content`) VALUES
(2, 'd1.jpg', 'Amanat Nur Rochman', 'Test Data Testimony', 'Hanya Data Percobaan'),
(8, '1.jpg', 'Rida Shafira Setyawan', 'Test Data Testimony Dari Rida', 'Hanya Data Percobaan Update Dari Rida  '),
(11, 'd3.jpg', 'Takeshi Yamada', 'Test Data Testimony Dari Takeshi', 'Data Percobaan Dari Takeshi ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `users_name` varchar(35) NOT NULL,
  `user_contact` char(15) NOT NULL,
  `users_email` varchar(50) NOT NULL,
  `users_password` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `users_name`, `user_contact`, `users_email`, `users_password`, `created_at`, `last_login`) VALUES
(1, 'Admin', '+6289672176132', 'admin@gmail.com', '$2y$10$FZXhQzp.cpCyl9cSK9r1ee3dreXW74t8vtdd51JzTUVPXHGbFitwS', '2021-12-07 19:58:03', '2021-12-09 07:30:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `config_page`
--
ALTER TABLE `config_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `testimoni_id` (`testimoni_id`);

--
-- Indexes for table `config_web`
--
ALTER TABLE `config_web`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `config_page`
--
ALTER TABLE `config_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `config_web`
--
ALTER TABLE `config_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `config_page`
--
ALTER TABLE `config_page`
  ADD CONSTRAINT `config_page_ibfk_1` FOREIGN KEY (`testimoni_id`) REFERENCES `testimoni` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
